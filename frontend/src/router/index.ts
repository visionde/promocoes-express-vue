import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    // route level code-splitting
    // this generates a separate chunk (defaltView.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "default-view" */ "../layouts/Default.vue"),
    children: [
      {
        path: "/",
        name: "Home Page",
        meta: {
          title: "Página inicial",
        },
        component: () =>
          import(
            /* webpackChunkName: "home-page" */ "../views/ecommerce/HomePage.vue"
          ),
      },
      {
        path: "/identification",
        name: "Login e Cadastro",
        component: () =>
          import(
            /* webpackChunkName: "identification" */ "../views/auth/Identification.vue"
          ),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  linkActiveClass: "active",
  linkExactActiveClass: "active",
  base: process.env.BASE_URL,
  routes,
});

/* router.beforeEach((to, from, next) => {
  document.title = `Promoções express - ${to.meta.title}`;
}); */

export default router;
