const conexao = require('../Configs/db');

exports.getProduto = async() => {

    var result = await conexao('produto')
        .select()
        .innerJoin('produto_categoria', 'produto.produto_categoria_id', 'produto_categoria.prod_categoria_id')
        .where('produto.produto_visivel', 1)

        const promessas = result.map(async value => 

             resultEsp = await conexao('produto_espeficacao')
                .select()
                .where('produto_espeficacao.prod_espeficacao_visivel', 1)
                .where('produto_espeficacao.prod_espeficacao_produto_id', value.produto_id)
             
        );
        const especificacao = await Promise.all(promessas);

            for(var i = 0; i < result.length; i++){
                
                var itens = new Array(); 

                for(var j=0; j<especificacao.length; j++) {

                    for(var k=0; k< especificacao[j].length; k++){
                        if (result[i]["produto_id"] === especificacao[j][k]["prod_espeficacao_produto_id"]) {
                            itens.push(especificacao[j][k]);
                        }
                    }
                }

                result[i]['especificacao'] = itens;
            }
                 
        if(result){
            return result;
        } else {
            throw 'Erro ao listar !!';
        }
} 


exports.getProdutoID = async(id) => {

    var result = await conexao('produto')
        .select()
        .innerJoin('produto_categoria', 'produto.produto_categoria_id', 'produto_categoria.prod_categoria_id')
        .where('produto.produto_visivel', 1)
        .where('produto.produto_id', id)

        const promessas = result.map(async value => 

             resultEsp = await conexao('produto_espeficacao')
                .select()
                .where('produto_espeficacao.prod_espeficacao_visivel', 1)
                .where('produto_espeficacao.prod_espeficacao_produto_id', value.produto_id)
             
        );
        const especificacao = await Promise.all(promessas);

            for(var i = 0; i < result.length; i++){
                
                var itens = new Array(); 

                for(var j=0; j<especificacao.length; j++) {

                    for(var k=0; k< especificacao[j].length; k++){
                        if (result[i]["produto_id"] === especificacao[j][k]["prod_espeficacao_produto_id"]) {
                            itens.push(especificacao[j][k]);
                        }
                    }
                }

                result[i]['especificacao'] = itens;
            }
                 
        if(result){
            return result;
        } else {
            throw 'Erro ao listar !!';
        }
}