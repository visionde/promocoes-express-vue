const router =  require('express').Router();

router.use('/usuario', require('./UsuarioRoute'));
router.use('/api/produto', require('./ProdutoRoute'));
router.use('/api/produtoCategoria', require('./ProdutoCategoriaRoute'));

module.exports = router;