const router =  require('express').Router();

const ProdutoController = require('../Controllers/ProdutoController');
const authMiddleware = require('../Middlewares/auth');

router.get('/', ProdutoController.get);
router.get('/:id', ProdutoController.getById); 

module.exports = router;

