const router =  require('express').Router();

const ProdutoCategoriaController = require('../Controllers/ProdutoCategoriaController');
const authMiddleware = require('../Middlewares/auth');

router.get('/', ProdutoCategoriaController.get);
router.get('/:id', ProdutoCategoriaController.getById); 

module.exports = router;

