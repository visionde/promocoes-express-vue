const ProdutoModel = require('../Model/ProdutoModel');
//const jwt = require('../configs/authenticate');

exports.get = async (req, res ) => {

    try {
        const result = await ProdutoModel.getProduto();
        return res.json(result);

    } catch (error) {

        return res.status(400).json({
            error
        })
    }
    
}

exports.getById = async (req, res ) => {

    const {id} = req.params;

    try {
        const result = await ProdutoModel.getProdutoID(id);
        return res.json(result);

    } catch (error) {

        return res.status(400).json({
            error
        })
    }
    
}


