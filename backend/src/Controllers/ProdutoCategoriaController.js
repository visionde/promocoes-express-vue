const ProdutoCategoriaModel = require('../Model/ProdutoCategoriaModel');
//const jwt = require('../Configs/authenticate');

exports.get = async (req, res ) => {

    try {
        const result = await ProdutoCategoriaModel.getCategoria();
        return res.json(result);

    } catch (error) {

        return res.status(400).json({
            error
        })
    }
    
}

exports.getById = async (req, res ) => {

    const {id} = req.params;

    try {
        const result = await ProdutoCategoriaModel.getCategoriaProdutoID(id);
        return res.json(result);

    } catch (error) {

        return res.status(400).json({
            error
        })
    }
    
}


