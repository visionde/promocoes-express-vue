const connection = require('knex')({
	client: 'mysql',
	connection: {
        host: 'localhost',
        port: 3306,
        user: 'root',
        database : 'promocoes-express',
        password: ''
	}
});

module.exports = connection;