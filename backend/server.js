// Importa e Starta o ExpressJs
var express = require('express');
var app = express();
app.use(express.json());

var cors = require('cors');
app.use(cors());

app.get('/', function(req, res){
    res.send("Hello from the root application URL");
});

app.use('/', require('./src/Routes'));

app.use(express.urlencoded({ extended: true }));

app.set('port', process.env.PORT || 3333);

app.listen(app.get('port'), function() {
	console.log('Start!!!, Port: ' + app.get('port'));
});